import config
from app import app

app.run(host=config.IP, port=config.PORT, debug=config.DEBUG)
