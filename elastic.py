import requests
from app import app
from app.settings.models import get_settings, get_index_settings, get_enabled_indexes
import json


class Request(object):
    def __init__(self, host, port, ssl=False, protected=False, user=None, password=None):
        self.host = host
        self.port = port
        self.ssl = ssl
        self.protected = protected
        self.user = user
        self.password = password
        if self.ssl:
            self.url = 'https://' + self.host + ':' + str(self.port)
        else:
            self.url = 'http://' + self.host + ':' + str(self.port)

        self.s = requests.session()

    def get(self, target, params=None):
        try:
            if params is not None:
                r = self.s.get(self.url + target, params=params, timeout=2)
            else:
                r = self.s.get(self.url + target)
            return r.text
        except:
            return None

    def post(self, target, data=None):
        try:
            if data is not None:
                r = self.s.post(self.url + target, data=data, timeout=2)
            else:
                r = self.s.post(self.url + target)
            return r.text
        except:
            return None

    def alert_post(self, target, post_name, post_data, post_protected=False, post_user=None, post_password=None):
        try:
            data = {post_name: post_data}

            if post_protected:
                r = self.s.post(target, data, auth=(post_user, post_password), timeout=10)
            else:
                r = self.s.post(target, data, timeout=10)

            if r.status_code:
                return True
            else:
                return False
        except:
            return None

    def check_connection(self):
        try:
            if self.protected is not None:
                r = self.s.get(self.url, auth=(self.user, self.password), timeout=2)
            else:
                r = self.s.get(self.url, timeout=2)
            if r.status_code == 200:
                return True
        except:
            return None

    def set_ssl(self, value):
        if value in [True, False]:
            self.ssl = value
        else:
            return None

    def update_connection(self, host, port):
        self.host = host
        self.port = port
        if self.ssl:
            self.url = 'https://' + self.host + ':' + str(self.port)
        else:
            self.url = 'http://' + self.host + ':' + str(self.port)
        return True

    def set_protected(self, value, user=None, password=None):
        if value:
            self.protected = True
            self.user = user
            self.password = password
        elif value is False:
            self.protected = False
        else:
            return None


class Elastic(Request):
    def get_indexes_info(self):
        return Request.get(self, '/_cat/indices?v')

    def search_all(self, index, offset=0, sort="desc"):
        with app.app_context():
            enabled_indexes = get_enabled_indexes()
            if index not in enabled_indexes:
                return None

            number_of_elements = str(get_settings('ge')['ge-ele'])
            index_settings = get_index_settings(index)
            try:

                r = Request.get(self, '/' + str.lower(index) + '/_search',
                                params={"sort": "@timestamp:" + sort, "from": int(offset) * int(number_of_elements),
                                        "size": number_of_elements})
                data = json.loads(r)

                def get(dane, path):
                    for step in path:
                        dane = dane[step]
                    return dane

                header = []

                for setting in index_settings:
                    header.append([setting['name'], setting['alias']])
                results = []
                try:
                    if get(data, ['hits', 'total']) == 0:
                        return None
                    for hit in data['hits']['hits']:
                        hit_array = []
                        for setting in index_settings:
                            hit_array.append(get(hit, setting['target']))
                        results.append(hit_array)
                except:
                    return None
            except:
                return None
        return header, results

    def search_query(self, index, query, offset=0, sort="desc"):
        with app.app_context():

            enabled_indexes = get_enabled_indexes()
            if index not in enabled_indexes:
                return None

            number_of_elements = str(get_settings('ge')['ge-ele'])
            index_settings = get_index_settings(index)

            from parse import replace_aliases_and_targets, ret_alias_dict, split_keep_delimiters
            tmp_query = split_keep_delimiters(query, [')', '(', ':', ' '])
            alias_target = ret_alias_dict(index_settings)
            tmp_query = replace_aliases_and_targets(tmp_query, alias_target)
            try:
                r = Request.get(self, '/' + str.lower(index) + '/_search',
                                params={"sort": "@timestamp:" + sort, "from": int(offset) * int(number_of_elements),
                                        "size": number_of_elements, "q": tmp_query})
                data = json.loads(r)

                def get(dane, path):
                    for step in path:
                        dane = dane[step]
                    return dane

                header = []

                for setting in index_settings:
                    header.append([setting['name'], setting['alias']])

                results = []
                try:
                    if get(data, ['hits', 'total']) == 0:
                        return None
                    for hit in data['hits']['hits']:
                        hit_array = []
                        for setting in index_settings:
                            hit_array.append(get(hit, setting['target']))
                        results.append(hit_array)
                except:
                    return None
            except:
                return None
        return header, results

    def get_elastic_info(self):
        r = Request.get(self, '/_cat/nodes', params={"v": "", "h": "name,host,load,u,rp,hc,d"})
        return r
