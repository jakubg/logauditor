# -*- coding: utf-8 -*-

import sys
from app import app
import json
import logging
from config import WORKER_LOG_PATH, ALLOWED_CRON_TIMES
from app.alerts.models import add_alert
from app.scheduler.models import get_schedulers_with_index
from app.settings.models import get_enabled_indexes, get_settings
import app.alerts.constants as ALERT
from elastic import Elastic

# Enable logging
logging.getLogger('spam_application')
FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
logging.basicConfig(filename=WORKER_LOG_PATH, level=logging.INFO, format=FORMAT)

# Logging started
logging.info('Application started.')

# Get cycle from passed argument
try:
    run_cycle = int(sys.argv[1])
except IndexError:
    print 'Pass time as argument!'
    print 'Allowed times for worker: %r' % ALLOWED_CRON_TIMES
    logging.info('No argument was passed!')
    raw_input('Hit key for exit.')
    exit(1)

if run_cycle in ALLOWED_CRON_TIMES:
    logging.info('Cycle set to: %d' % run_cycle)
else:
    logging.error('Wrong arg was passed!')
    exit(1)

# Get ES Settings
with app.app_context():
    settings = get_settings('es')  # Save them as dictionary
    enabled_indexes = get_enabled_indexes()  # Save as list
    logging.info('Loaded basic configuration')

# Setup Elastic connection
connection = Elastic(settings['es-ip'], settings['es-port'])

if settings['es-ssl']:  # Enable SSL when needed
    connection.set_ssl(True)
if settings['es-prot']:  # Enable HTTP Auth when needed
    connection.set_protected(True, settings['es-user'], settings['es-password'])

# Check ES connection
if connection.check_connection():
    logging.info('Successfully connected to ES')
else:
    logging.error('Can\'t connect to ES server!')
    exit(1)

# For each index get schedulers
with app.app_context():
    for index in enabled_indexes:
        app.extensions[index + '_schedulers'] = get_schedulers_with_index(index)
        logging.info('Loaded schedulers for %s' % index)

# Collect data for each index
for index in enabled_indexes:
    # Collect data for each schedulers using saved configuration in app.extensions
    for scheduler in app.extensions[index + '_schedulers']:
        if scheduler['cycle'] == run_cycle:
            output = connection.post('/' + index + '/_search', scheduler['command'])
            if json.loads(output)['hits']['total'] == 0:
                logging.info('No data for scheduler "' + scheduler['name'] + '"')
                continue
            with app.app_context():
                if scheduler['post_status']:
                    if scheduler['post_protected']:
                        if connection.alert_post(scheduler['post_addr'], scheduler['post_name'], output, True,
                                                 scheduler['post_username'], scheduler['post_password']):
                            add_alert(scheduler['id'], output, ALERT.SENT)
                            logging.info('Alert for "' + scheduler['name'] + '" sent')
                        else:
                            add_alert(scheduler['id'], output, ALERT.ERROR)
                            logging.error('Alert for "' + scheduler['name'] + '" not sent!')
                    else:
                        if connection.alert_post(scheduler['post_addr'], scheduler['post_name'], output):
                            add_alert(scheduler['id'], output, ALERT.SENT)
                            logging.info('Alert for "' + scheduler['name'] + '" sent')
                        else:
                            add_alert(scheduler['id'], output, ALERT.ERROR)
                            logging.error('Alert for "' + scheduler['name'] + '" not sent!')
                else:
                    add_alert(scheduler['id'], output, ALERT.NOT_SENT)
                    logging.info('Alert for "' + scheduler['name'] + '" added to database.')

# Tasks completed, log it
logging.info('Application completed work, safe exit.')
