from app.alerts.models import Alert
from database import db


def remove_alert(alert_id):
    db.session.query(Alert).filter(Alert.id == alert_id).filter(Alert.archived == 1).delete()
    db.session.commit()
    return True


def remove_alerts_by_date(start, end):
    db.session.query(Alert).filter((Alert.archived == 1) & (Alert.date > start) & (Alert.date <= end)).delete()
    db.session.commit()
    return True
