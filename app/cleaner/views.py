from flask import Blueprint, render_template, request
from app.cleaner.models import remove_alerts_by_date
from app.settings.models import get_enabled_indexes

mod = Blueprint('cleaner', __name__, url_prefix='/cleaner')


@mod.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form.get('cl-alias') is not None:
            daterange = request.form.get('daterange')
            if daterange is not None:
                date = daterange.split(' - ')
                start = date[0].split('/')
                end = date[1].split('/')
                date_start = start[2] + '-' + start[0] + '-' + start[1]
                date_end = end[2] + '-' + end[0] + '-' + end[1]

                print date_end, date_start

                remove_alerts_by_date(date_start, date_end)

        return render_template("cleaner/cleaner.html", title="Cleaner")
    else:
        return render_template("cleaner/cleaner.html", title="Cleaner")
