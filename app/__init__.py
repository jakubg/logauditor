import os.path

from flask import Flask, render_template
from Helper import Elastic
from database import db
from config import APP_DEV, POST_TEST
import requests

app = Flask(__name__)
app.config.from_object('config')

db.init_app(app)
es = Elastic

Request = requests.session()

from app.settings.models import Setting, default_es_settings, default_ge_settings, get_settings, \
    default_index_settings, get_enabled_indexes, get_index_settings
from app.alerts.models import Alert, Scheduler, alerts_default
from app.scheduler.models import scheduler_default

with app.app_context():
    if not os.path.isfile('app.db'):
        db.create_all()

        default_ge_settings()
        default_es_settings()

        if APP_DEV:
            scheduler_default()
            alerts_default()
            default_index_settings()

    from elastic import Elastic

    connection_settings = get_settings('es')
    Connection = Elastic(connection_settings['es-ip'], connection_settings['es-port'])


@app.errorhandler(404)
def not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(405)
def not_allowed(e):
    return render_template('405.html'), 405


@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500


@app.route("/robots.txt")
def robots_txt():
    return render_template("robots.txt")


from app.index.views import mod as indexModule
from app.alerts.views import mod as alertsModule
from app.cleaner.views import mod as cleanerModule
from app.scheduler.views import mod as schedulerModule
from app.search.views import mod as searchModule
from app.settings.views import mod as settingsModule

app.register_blueprint(indexModule)
app.register_blueprint(cleanerModule)
app.register_blueprint(schedulerModule)
app.register_blueprint(searchModule)
app.register_blueprint(settingsModule)
app.register_blueprint(alertsModule)

"""
from app.docs.views import mod as docsModule
app.register_blueprint(docsModule)
"""

if POST_TEST:
    from app.post.views import mod as postModule

    app.register_blueprint(postModule)
