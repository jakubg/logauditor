from flask import Blueprint, render_template, request, url_for, redirect
import app.settings.models as model
from app.settings.models import set_index_status, get_all_indexes, get_index_settings, \
    remove_index_from_database, add_index, update_index_settings
import json
from app import Connection
import parse

mod = Blueprint('settings', __name__, url_prefix='/settings')


@mod.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if Connection.check_connection():
            conn = Connection.get_indexes_info()
            indexes_avail = parse.parse_avail_indexes(conn)
            error = False
        else:
            indexes_avail = []
            error = 'No connection to Elasticsearch! Check your settings.'

        if request.form.get('enable'):
            index_name = request.form.get('enable')
            if set_index_status(index_name, True):
                return '', 200
            else:
                return '', 400
        if request.form.get('disable'):
            index_name = request.form.get('disable')
            if set_index_status(index_name, False):
                return '', 200
        if request.form.get('trash'):
            index_name = request.form.get('trash')
            if remove_index_from_database(index_name):
                return '', 200
        if request.form.get('add_index'):
            index_name = request.form.get('add_index')
            if index_name in indexes_avail:
                add_index(index_name)
                return '', 200
        parsed_form = parse.parse_post_settings(request.form)
        if parsed_form is not None:
            model.update_settings(parsed_form[0], parsed_form[1])
        ge = model.get_settings('ge')
        es = model.get_settings('es')
        ge.update(es)
        Connection.update_connection(es['es-ip'], es['es-port'])
        indexes = get_all_indexes()
        return render_template("settings/settings.html", title="Settings",
                               settings=json.dumps(ge, ensure_ascii=False), active='es', indexes=indexes, error=error)
    else:
        ge = model.get_settings('ge')
        es = model.get_settings('es')
        ge.update(es)
        if Connection.check_connection():
            conn = Connection.get_indexes_info()
            indexes_avail = parse.parse_avail_indexes(conn)
            indexes = get_all_indexes(True)
            indexes_avail = set(indexes_avail) - set(indexes)
            indexes = get_all_indexes()
            error = False
        else:
            indexes = get_all_indexes()
            indexes_avail = []
            error = 'No connection to Elasticsearch! Check your settings.'
        return render_template("settings/settings.html", title="Settings",
                               settings=json.dumps(ge, ensure_ascii=False), active='es', indexes=indexes,
                               indexes_avail=indexes_avail, error=error)


@mod.route('/<string:name>', methods=['GET', 'POST'])
def indexes(name):
    if request.method == 'POST':
        settings = parse.parse_index_settings(request.form)
        update_index_settings(name, settings)
        return redirect(url_for('settings.index') + '#indexes')
    else:
        if name not in get_all_indexes(True):
            raise SystemError('Index does not exists!')

        settings = json.dumps(get_index_settings(name))
        return render_template("settings/indexes.html", title="Settings", index_name=name, settings=settings)
