from database import db
from ast import literal_eval
from config import APP_DEV
from app.scheduler.models import remove_scheduler_with_index


class Setting(db.Model):
    __tablename__ = 'setting'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    settings = db.Column(db.String(1000))

    def __init__(self, name=None, settings=None):
        self.name = name
        self.settings = settings

    def __repr__(self):
        return "Settings<name='%s', settings='%s'>" % (self.name, self.settings)


class Index(db.Model):
    __tablename__ = 'index'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    enabled = db.Column(db.Boolean, default=False)
    schema = db.Column(db.UnicodeText)

    def __init__(self, name, schema=None, enabled=False):
        self.name = name
        self.schema = schema
        self.enabled = enabled

    def __repr__(self):
        return "Indexes<name='%s', enabled='%s'>" % (self.name, self.enabled)


def default_es_settings():
    es = Setting('es', '{"es-ip": "192.168.1.117", "es-port": 9200, "es-ssl": False, '
                       '"es-prot": False, "es-user": None, "es-password": None}')
    db.session.add(es)
    db.session.commit()
    return True


def default_ge_settings():
    ge = Setting('ge', '{"ge-ele": 150, "ge-maxalerts": 50, "ge-maxschedulers": 50}')
    db.session.add(ge)
    db.session.commit()
    return True


def default_index_settings():
    if not APP_DEV:
        return True
    index1 = Index(
            "access", "[{'name': 'Timestamp', 'alias': 'time', 'target': ['_source', '@timestamp']},"
                      "{'name': 'Host', 'alias': 'host', 'target': ['_source', 'host']},"
                      "{'name': 'Client', 'alias': 'client', 'target': ['_source', '@fields', 'remote_addr']},"
                      "{'name': 'Req. time', 'alias': 'req', 'target': ['_source', '@fields', 'request_time']},"
                      "{'name': 'Status', 'alias': 'status', 'target': ['_source', '@fields', 'status']},"
                      "{'name': 'URI', 'alias': 'uri', 'target': ['_source', '@fields', 'request_uri']}]",
            True)
    db.session.add(index1)
    db.session.commit()
    return True


def add_index(name):
    index = Index(name)
    db.session.add(index)
    db.session.commit()
    return True


def get_index_settings(name):
    try:
        query = db.session.query(Index).filter_by(name=name).first()
        return literal_eval(query.schema)
    except ValueError:
        return [{"name": '', "alias": '', "target": ''}]


def get_enabled_indexes():
    query = db.session.query(Index).filter_by(enabled=True)
    names = []
    for item in query:
        names.append(item.name)
    return names


def get_all_indexes(only_name=False):
    query = db.session.query(Index)
    names = []
    for item in query:
        if only_name:
            names.append(item.name)
        else:
            names.append([item.name, item.enabled])
    return names


def get_settings(name):
    query = db.session.query(Setting).filter_by(name=name).first()
    return literal_eval(query.settings)


def update_settings(name, settings):
    db.session.query(Setting).filter(Setting.name == name).update({'settings': str(settings)})
    db.session.commit()
    return True


def update_index_settings(name, settings):
    db.session.query(Index).filter(Index.name == name).update({'schema': str(settings)})
    db.session.commit()
    return True


def set_index_status(name, status):
    db.session.query(Index).filter(Index.name == name).update({'enabled': status})
    db.session.commit()
    return True


def remove_index_from_database(name):
    remove_scheduler_with_index(name)
    db.session.query(Index).filter(Index.name == name).delete()
    db.session.commit()
    return True
