from flask import Blueprint, render_template

mod = Blueprint('docs', __name__, url_prefix='/docs')


@mod.route('/')
def index():
    return render_template("docs/docs.html", title="Documentation")
