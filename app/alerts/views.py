from flask import Blueprint, render_template, request
from app.alerts.models import get_alerts, get_archived_alerts, get_alert, archive_alert, remove_alert, \
    archive_all_alerts
from app.settings.models import get_settings

mod = Blueprint('alerts', __name__, url_prefix='/alerts')


@mod.route('/')
def index():
    number_of_alerts = get_settings('ge')['ge-maxalerts']
    alerts = get_alerts(number_of_alerts)
    return render_template("alerts/alerts.html", title="Alerts", alerts=alerts)


@mod.route('/archive', methods=['GET', 'POST'])
def archive():
    if request.method == 'POST':
        if request.form.get('archive'):
            alert_id = request.form.get('archive')
            if archive_alert(alert_id):
                return '', 200
        if request.form.get('trash'):
            alert_id = request.form.get('trash')
            if remove_alert(alert_id):
                return '', 200
        if request.form.get('archive_all'):
            if archive_all_alerts():
                return '', 200
        else:
            return '', 400
    else:
        number_of_alerts = get_settings('ge')['ge-maxalerts']
        alerts_archived = get_archived_alerts(number_of_alerts)
        return render_template("alerts/archive.html", title="Alerts archive", alerts=alerts_archived)


@mod.route('/raw/<int:id>')
def show(id):
    alert = get_alert(id)
    if alert is not False:
        return render_template("alerts/raw.html", alert=str(alert)), 200
    else:
        raise ValueError('No data')
