# -*- coding: UTF-8 -*-
from database import db
from app.alerts import constants as ALERT
import datetime
from config import APP_DEV


class Alert(db.Model):
    __tablename__ = 'alert'
    id = db.Column(db.Integer, primary_key=True)
    schedule_id = db.Column(db.Integer)
    date = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    alert = db.Column(db.UnicodeText)
    post = db.Column(db.SmallInteger, default=ALERT.SENT)
    archived = db.Column(db.Boolean, default=False)

    def __init__(self, schedule_id, archived=False, alert=None, post=None):
        self.post = post
        self.alert = alert
        self.archived = archived
        self.schedule_id = schedule_id


class Scheduler(db.Model):
    __tablename__ = 'scheduler'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), nullable=False, unique=True)
    target = db.Column(db.String(128), nullable=False)
    cycle = db.Column(db.Integer, default=5, nullable=False)
    command = db.Column(db.UnicodeText, nullable=False)
    status = db.Column(db.Boolean, default=False)
    post_status = db.Column(db.Boolean, default=False)
    post_addr = db.Column(db.String(256))
    post_name = db.Column(db.String(50), default='output')
    post_protected = db.Column(db.Boolean, default=False)
    post_username = db.Column(db.String(50))
    post_password = db.Column(db.String(50))

    def __init__(self, name, cycle, command, target, status=False, post_status=False, post_addr=None, post_name=None,
                 post_protected=None, post_username=None, post_password=None):
        self.name = name
        self.cycle = cycle
        self.command = command
        self.target = target
        self.status = status
        self.post_status = post_status
        self.post_addr = post_addr
        self.post_name = post_name
        self.post_protected = post_protected
        self.post_username = post_username
        self.post_password = post_password


def alerts_default():
    if not APP_DEV:
        return True
    alert1 = Alert(1, False, '{"test1": "somedata"}', True)
    db.session.add(alert1)
    alert2 = Alert(1, False, '{"test1": "somedata"}', True)
    db.session.add(alert2)
    alert3 = Alert(2, False, '{"test1": "somedata"}', True)
    db.session.add(alert3)
    alert4 = Alert(2, False, '{"test1": "somedata"}', True)
    db.session.add(alert4)
    alert5 = Alert(2, True, '{"test2": "somedata"}', False)
    db.session.add(alert5)
    db.session.commit()
    return True


def add_alert(schedule_id, alert, post):
    alert = Alert(schedule_id, False, alert, post)
    db.session.add(alert)
    db.session.commit()
    return True


def get_alerts(number_of_alerts):
    output = []
    for alerts, schedulers in db.session.query(Alert, Scheduler).filter(Alert.archived == 0). \
            order_by(Alert.id.desc()).outerjoin(Scheduler, Alert.schedule_id == Scheduler.id).limit(
            number_of_alerts):
        output.append([alerts.id, str(alerts.date)[0:-7], schedulers.name, alerts.post, schedulers.target])
    return output


def get_alert(alert_id):
    return db.session.query(Alert.alert).filter(Alert.id == alert_id).first()[0]


def get_archived_alerts(number_of_alerts):
    output = []
    for alerts, schedulers in db.session.query(Alert, Scheduler).filter(Alert.archived == 1). \
            order_by(Alert.id.desc()).outerjoin(Scheduler, Alert.schedule_id == Scheduler.id).limit(
            number_of_alerts):
        output.append([alerts.id, str(alerts.date)[0:-7], schedulers.name, alerts.post, schedulers.target])
    return output


def archive_alert(alert_id):
    db.session.query(Alert).filter(Alert.id == alert_id).update({'archived': True})
    db.session.commit()
    return True


def remove_alert(alert_id):
    db.session.query(Alert).filter(Alert.id == alert_id).delete()
    db.session.commit()
    return True


def remove_alerts_with_schedid(sched_id):
    db.session.query(Alert).filter(Alert.schedule_id == sched_id).delete()
    db.session.commit()
    return True


def archive_all_alerts():
    db.session.query(Alert).filter(Alert.archived == 0).update({'archived': True})
    db.session.commit()
    return True
