from flask import Blueprint, render_template, request
from app.settings.models import get_enabled_indexes
from app import Connection

mod = Blueprint('search', __name__, url_prefix='/search')


@mod.route('/', methods=['GET', 'POST'])
def index():
    indexes = get_enabled_indexes()
    if request.method == 'POST':
        query = ' '.join(request.form.get('query').split())
        index = request.form.get('index')

        if index not in indexes:
            error = 'Can\'t find working index. Check settings!'
            return render_template("search/search.html", title="Search", indexes=indexes, old_query=query, error=error)
        if len(query) > 2:
            search_results = Connection.search_query(str(index), query)
        else:
            search_results = Connection.search_all(str(index))
        if search_results is not None:
            return render_template("search/search.html", title="Search", indexes=indexes, old_query=query,
                                   header=search_results[0], results=search_results[1])
        else:
            error = 'Can\'t find working index. Check settings!'
            return render_template("search/search.html", title="Search", indexes=indexes, old_query=query, error=error)
    else:
        if len(indexes) < 1:
            error = 'Can\'t find working index. Check settings!'
            return render_template("search/search.html", title="Search", message='No indexes enabled!', error=error)
        if request.args.get('page'):
            offset = int(request.args.get('page'))
            if offset > 0:
                offset = offset
            else:
                offset = 0
        else:
            offset = 0

        if offset > 0:
            offset_next = offset + 1
        else:
            offset_next = 1
        if offset > 1:
            offset_prev = offset - 1
        else:
            offset_prev = 0

        search_results = Connection.search_all(str(indexes[0]), offset=offset)
        if search_results is not None:
            return render_template("search/search.html", title="Search", indexes=indexes,
                                   header=search_results[0], results=search_results[1], pager=True,
                                   offset_next=offset_next, offset_prev=offset_prev)
        else:
            error = 'Can\'t find working index. Check settings!'
            return render_template("search/search.html", title="Search", error=error)
