"""
from app.alerts.models import Alert, Scheduler
from app import db
import sys


def get_archived_alerts():
    try:
        return db.session.query(Alerts.id).filter(Alerts.archived == True).count()
    except:
        print 'Error: ', sys.exc_info()[0]
        return False


def get_all_alerts():
    try:
        return db.session.query(Alerts.id).count()
    except:
        print 'Error: ', sys.exc_info()[0]
        return False


def get_all_schedulers():
    try:
        return db.session.query(Schedulers.id).count()
    except:
        print 'Error: ', sys.exc_info()[0]
        return False


def get_enabled_schedulers():
    try:
        return db.session.query(Schedulers.id).filter(Schedulers.status == True).count()
    except:
        print 'Error: ', sys.exc_info()[0]
        return False
"""
