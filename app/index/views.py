from flask import Blueprint, redirect, url_for

""" Disabled -- no time
from app.index.models import get_archived_alerts, get_all_alerts, get_enabled_schedulers, get_all_schedulers
from app import Connection
from parse import parse_elastic_info
from pprint import pprint



@mod.route('/', methods=['GET'])
def index():
    es_info = parse_elastic_info(Connection.get_elastic_info())
    all_alerts = get_all_alerts()
    arch_alerts = get_archived_alerts()
    get_enabled_sched = get_enabled_schedulers()
    get_all_sched = get_all_schedulers()
    alerts = {"all": all_alerts,
              "archived": arch_alerts,
              "unarchived": all_alerts - arch_alerts
              }
    sched = {"all": get_all_sched, "enabled": get_enabled_sched,
             "disabled": get_all_sched - get_enabled_sched}
    return render_template('index/index.html', title='Index', es_head=es_info[0], es_info=es_info[1], alerts=alerts,
                           sched=sched)
"""

mod = Blueprint('index', __name__, url_prefix='/')


@mod.route('/')
def index():
    return redirect(url_for('search.index'))
