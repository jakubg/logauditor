# -*- coding: UTF-8 -*-
from database import db
from config import APP_DEV
from app.alerts.models import Scheduler, remove_alerts_with_schedid


def scheduler_default():
    if not APP_DEV:
        return True
    scheduler1 = Scheduler('Any 404 on 192.168.1.154', 5,
                           '{"query":{"filtered":{"query":{"bool":{"must":[{"match":{"@fields.status":"404"}},'
                           '{"match":{"host":"192.168.1.204"}}]}},'
                           '"filter":{"range":{"@timestamp":{"from":"now-5m","to":"now"}}}}}}',
                           'access', False, True,
                           'http://192.168.1.117/post/', 'output')
    db.session.add(scheduler1)
    scheduler2 = Scheduler('Time over 1sec on 192.168.1.154', 60,
                           '{"query":{"filtered":{"query":{"bool":{"must":[{"match":{"host":"192.168.1.204"}}]}},'
                           '"filter":{"range":{"@timestamp":{"from":"now-60m","to":"now"},'
                           '"@fields.request_time":{"gte":1}}}}}}',
                           'access', False, False,
                           'http://192.168.1.117/post/', 'output')
    db.session.add(scheduler2)
    db.session.commit()
    return True


def add_scheduler(data):
    new = Scheduler(data['sh-name'], data['sh-cycle'], data['sh-script'], data['sh-target'], data['sh-enabled'],
                    data['sh-post-enabled'],
                    data['sh-post-addr'], data['sh-post-output'], data['sh-post-protected'], data['sh-post-username'],
                    data['sh-post-password'])
    db.session.add(new)
    db.session.commit()
    return True


def update_scheduler_using_id(scheduler_id, data):
    db.session.query(Scheduler).filter(Scheduler.id == scheduler_id).update(
            {"name": data['sh-name'], "cycle": data['sh-cycle'], "command": data['sh-script'],
             "target": data['sh-target'],
             "status": data['sh-enabled'], "post_status": data['sh-post-enabled'], "post_addr": data['sh-post-addr'],
             "post_name": data['sh-post-output'], "post_protected": data['sh-post-protected'],
             "post_username": data['sh-post-username'], "post_password": data['sh-post-password']})
    db.session.commit()
    return True


def update_scheduler_using_name(name, data):
    db.session.query(Scheduler).filter(Scheduler.name == name).update(
            {"name": data['sh-name'], "cycle": data['sh-cycle'], "command": data['sh-script'],
             "target": data['sh-target'], "status": data['sh-enabled'], "post_status": data['sh-post-enabled'],
             "post_addr": data['sh-post-addr'],
             "post_name": data['sh-post-output'], "post_protected": data['sh-post-protected'],
             "post_username": data['sh-post-username'], "post_password": data['sh-post-password']})
    db.session.commit()
    return True


def get_schedulers_list(number_of_schedulers):
    output = []
    for instance in db.session.query(Scheduler).order_by(Scheduler.id).limit(number_of_schedulers):
        output.append([instance.id, instance.name, instance.status, instance.post_status,
                       instance.cycle, instance.target])
    return output


def get_scheduler(scheduler_id):
    for instance in db.session.query(Scheduler).filter(Scheduler.id == scheduler_id).order_by(Scheduler.id):
        return [instance.id, instance.name, instance.cycle, instance.command, instance.status,
                instance.post_status, instance.post_addr, instance.post_name, instance.post_protected,
                instance.post_username, instance.post_password, instance.target]


def remove_scheduler(scheduler_id):
    remove_alerts_with_schedid(scheduler_id)
    db.session.query(Scheduler).filter(Scheduler.id == scheduler_id).delete()
    db.session.commit()
    return True


def remove_scheduler_with_index(target):
    for instance in db.session.query(Scheduler).filter(Scheduler.target == target).order_by(Scheduler.id):
        remove_scheduler(instance.id)
    return True


def get_schedulers_with_index(index):
    array = []
    for instance in db.session.query(Scheduler).filter(Scheduler.status == 1).filter(Scheduler.target == index):
        array.append({"id": instance.id, "cycle": instance.cycle, "command": instance.command,
                      "post_status": instance.post_status, "post_addr": instance.post_addr, "name": instance.name,
                      "post_name": instance.post_name, "post_protected": instance.post_protected,
                      "post_username": instance.post_username, "post_password": instance.post_password})
    return array


def set_scheduler_status(scheduler_id, status):
    if status in [True, False]:
        db.session.query(Scheduler).filter(Scheduler.id == scheduler_id).update({"status": status})
        db.session.commit()
        return True
