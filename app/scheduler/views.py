from flask import Blueprint, render_template, request, redirect, url_for
from app.scheduler.models import get_schedulers_list, get_scheduler, set_scheduler_status, remove_scheduler, \
    add_scheduler, update_scheduler_using_id
from app.settings.models import get_settings, get_all_indexes
from parse import parse_scheduler, parse_existing_scheduler
import json
from config import ALLOWED_CRON_TIMES

mod = Blueprint('scheduler', __name__, url_prefix='/scheduler')

cron_times = ALLOWED_CRON_TIMES


@mod.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if request.form.get('enable'):
            sch_id = request.form.get('enable')
            if set_scheduler_status(sch_id, True):
                return '', 200
        if request.form.get('disable'):
            sch_id = request.form.get('disable')
            if set_scheduler_status(sch_id, False):
                return '', 200
        if request.form.get('remove'):
            sch_id = request.form.get('remove')
            if remove_scheduler(sch_id):
                return '', 200
        else:
            return '', 400
    else:
        number_of_schedulers = get_settings('ge')['ge-maxschedulers']
        schedulers = get_schedulers_list(number_of_schedulers)
        return render_template("scheduler/scheduler.html", title="Scheduler", schedulers=schedulers,
                               cron_times=cron_times)


@mod.route('/new', methods=['GET', 'POST'])
def new():
    if request.method == 'POST':
        data = parse_scheduler(request.form)
        add_scheduler(data)

        return redirect(url_for('scheduler.index'))
    else:
        enabled_indexes = get_all_indexes()
        return render_template("scheduler/new.html", title="Scheduler", enabled_indexes=enabled_indexes,
                               cron_times=cron_times)


@mod.route('/edit/<int:id>', methods=['GET', 'POST'])
def edit_id(id):
    if request.method == 'POST':
        data = parse_scheduler(request.form)
        update_scheduler_using_id(id, data)
        return redirect(url_for('scheduler.index'))
    else:
        instance = get_scheduler(id)
        settings = parse_existing_scheduler(instance)

        enabled_indexes = get_all_indexes()
        return render_template("scheduler/edit.html", sch_id=id, title="Scheduler edit",
                               enabled_indexes=enabled_indexes,
                               settings=json.dumps(settings, ensure_ascii=False), cron_times=cron_times)


@mod.route('/clone/<int:id>')
def clone_id(id):
    instance = get_scheduler(id)
    settings = parse_existing_scheduler(instance, name=False)
    if settings is None:
        raise SystemError('Something wrong with scheduler array.')

    enabled_indexes = get_all_indexes()
    return render_template("scheduler/clone.html", id=id, title="Scheduler clone", enabled_indexes=enabled_indexes,
                           settings=json.dumps(settings, ensure_ascii=False), cron_times=cron_times)


@mod.route('/clone', methods=['POST'])
def clone():
    data = parse_scheduler(request.form)
    add_scheduler(data)
    return redirect(url_for('scheduler.index'))
