import os
from config import ALLOWED_CRON_TIMES

path = os.path.dirname(os.path.realpath(__file__))

print '# LogAuditor START'
for cycle in ALLOWED_CRON_TIMES:
    print "*/%s * * * * %s/worker.py %s" % (str(cycle), path, str(cycle))
print '# LogAuditor END\n'
print '# LogAuditor AUTOSTART START'
print '@reboot %s %s/venv/bin/python %s/run.py' % ('/usr/bin/screen -dmS logauditor', path, path)
print '# LogAuditor AUTOSTART END'
