import requests
import Parse
import sys
import json


class Elastic(object):
    def __init__(self, host, port, ssl=False, protected=False, user=None, name=None):
        self.host = host
        self.port = port
        self.ssl = ssl
        self.protected = prot
        self.user = user
        self.name = name
        self.url = 'http://' + self.host + ':' + self.port

        self.connected = False

    def getindexinfo(self):
        request = self.get('/_cat/indices', 'v')
        return Parse.seindexparse(request)

    def get(self, path, params=None):
        try:
            request = requests.get(self.url + path, params=params)
            return request.text
        except:
            print 'Error: ', sys.exc_info()[0]
            return False

    def post(self, path, data, params=None):
        try:
            request = requests.post(self.url + path, data=data, params=params)
            return request.text
        except:
            print 'Error: ', sys.exc_info()[0]
            return False

    def delete(self, path, params=None):
        try:
            request = requests.post(self.url + path, params=params)
            return request.text
        except:
            print 'Error: ', sys.exc_info()[0]
            return False
