from config import ALLOWED_CRON_TIMES

def parse_post_settings(input):
    output = {}
    if input.get('es-tab') is not None:
        output["es-ip"] = input.get('es-ip')
        output["es-port"] = input.get('es-port')
        if input.get('es-ssl') is not None:
            output["es-ssl"] = True
        else:
            output["es-ssl"] = False
        if input.get('es-prot') is not None:
            output["es-prot"] = True
        else:
            output["es-prot"] = False
        output["es-user"] = input.get('es-user')
        output["es-password"] = input.get('es-password')
        return 'es', output
    elif input.get('ge-tab') is not None:
        output["ge-ele"] = input.get('ge-ele')
        output["ge-maxalerts"] = input.get('ge-maxalerts')
        output["ge-maxschedulers"] = input.get('ge-maxschedulers')
        return 'ge', output
    else:
        return None


def parse_scheduler_settings(input):
    output = {}
    if input.get('es-tab') is not None:
        output["es-ip"] = input.get('es-ip')
        output["es-port"] = input.get('es-port')
        if input.get('es-ssl') is not None:
            output["es-ssl"] = True
        else:
            output["es-ssl"] = False
        if input.get('es-prot') is not None:
            output["es-prot"] = True
        else:
            output["es-prot"] = False
        output["es-user"] = input.get('es-user')
        output["es-password"] = input.get('es-password')
        return 'es', output
    elif input.get('ge-tab') is not None:
        output["ge-ele"] = input.get('ge-ele')
        output["ge-maxalerts"] = input.get('ge-maxalerts')
        output["ge-maxschedulers"] = input.get('ge-maxschedulers')
        return 'ge', output
    else:
        return None


def parse_scheduler(input):
    output = {}
    if input.get('sh-tab') is not None:
        output["sh-name"] = input.get('sh-name')  # Skip check, already done
        output["sh-target"] = input.get('sh-target')  # Skip check, already done

        sh_enabled = True if input.get('sh-enabled') == "on" else False
        output["sh-enabled"] = sh_enabled
        output["sh-cycle"] = int(input.get('sh-cycle'))  # Get only value without m at the end
        if output["sh-cycle"] not in ALLOWED_CRON_TIMES:
            return None
        output["sh-script"] = input.get('sh-script')

        sh_post_enabled = True if input.get('sh-post-enabled') == "on" else False
        output["sh-post-enabled"] = sh_post_enabled
        output["sh-post-addr"] = input.get('sh-post-addr')
        output["sh-post-output"] = input.get('sh-post-output')  # Def set to output
        if len(output["sh-post-output"]) < 1:
            output["sh-post-output"] = "output"

        sh_post_protected = True if input.get('sh-post-protected') == "on" else False
        output["sh-post-protected"] = sh_post_protected
        output["sh-post-username"] = input.get('sh-post-username')
        output["sh-post-password"] = input.get('sh-post-password')

        return output
    else:
        return None


def parse_existing_scheduler(input, name=True):
    if len(input) != 12:
        return None
    if name is not True:
        input[1] = ""
    return {"sh-name": input[1], "sh-cycle": input[2], "sh-script": input[3],
            "sh-enabled": input[4], "sh-post-enabled": input[5], "sh-post-addr": input[6],
            "sh-post-output": input[7], "sh-post-protected": input[8], "sh-post-username": input[9],
            "sh-post-password": input[10], "sh-target": input[11]}


def parse_index_settings(input):
    settings_in = []
    settings_in_name = []
    settings_in_alias = []
    settings_in_target_temp = []

    def split_target(target):
        return target.split(',')

    if input.get('index_settings') is not None:
        for name in input.getlist('name[]'):
            settings_in_name.append(name)
        for alias in input.getlist('alias[]'):
            settings_in_alias.append(alias)
        for target in input.getlist('target[]'):
            settings_in_target_temp.append(target)

        for i in range(0, len(settings_in_name)):
            settings_in.append({"name": settings_in_name[i], "alias": settings_in_alias[i],
                                "target": split_target(settings_in_target_temp[i])})
        return settings_in


def split_keep_delimiters(string, delimiters):
    """
    Split string using delimiters, but put them to output list too.
    :param string: Input string used to split
    :param delimiters: Delimiters in list
    :return: Array of splited items with delimiters
    """
    output = []
    tmp = ""
    for char in string:
        if char in delimiters:
            if len(tmp) > 0:
                output.append(tmp)
                tmp = ""
            output.append(char)
        else:
            tmp += char
    if len(tmp) > 0:
        output.append(tmp)
    return output


def ret_alias_dict(dictionary):
    """
    Method used for build alias -> target dict
    :param dictionary: Dictionary from Settings
    :return: Dictionary with alias -> target values
    """
    output = {}
    for item in dictionary:
        output[item.get('alias')] = '.'.join(item.get('target'))
    return output


def replace_aliases_and_targets(input_array, alias_array):
    """
    Method used for replacing alias with target, and : with =
    :param input_array: Input array with splited values
    :param alias_array: Dictionary with alias -> target values
    :return: Joined string with replaced alias -> target, : -> = values
    """
    for i in range(1, len(input_array) - 1):
        if input_array[i] == ":" and input_array[i - 1] in alias_array:
            input_array[i] = "="
            input_array[i - 1] = alias_array[input_array[i - 1]]
    return ''.join(input_array)


def parse_elastic_info(input):
    array = input.split('\n')
    old = []
    output = []
    for line in array:
        old.append(line.split())
    head = ['Node name', 'Host name', 'Load average', 'Node uptime', 'Used total memory %', 'Used heap',
            'Available disk space']
    output.append(old[1:-1][0])
    return head, output


def parse_avail_indexes(input):
    try:
        index_list = input.split('\n')
    except AttributeError:
        index_list = []
    output = []
    for i in range(1, len(index_list) - 1):
        inner_list = index_list[i].split()
        output.append(inner_list[2])
    return output
