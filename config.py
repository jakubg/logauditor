import os
_basedir = os.path.abspath(os.path.dirname(__file__))

PORT = 5005
IP = '127.0.0.1'
DEBUG = False
POST_TEST = False
APP_DEV = False
WORKER_LOG_LVL = 'INFO'
WORKER_LOG_PATH = os.path.join(_basedir, 'log-worker.txt')
ALLOWED_CRON_TIMES = [5, 10, 15, 30, 60, 120, 240]

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'app.db')
DATABASE_CONNECT_OPTIONS = {}
SQLALCHEMY_TRACK_MODIFICATIONS = False
